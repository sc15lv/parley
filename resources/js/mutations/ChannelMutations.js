var mutations = {
    addChannels(state, channels) {
        channels.forEach((channel, index) => {
            Vue.set(state.channels, channel.id, channel);
            Vue.set(state.channel_messages, channel.id, []);
            Vue.set(state.channel_unreads, channel.id, 0);
            Vue.set(state.channel_users, channel.id, []);
        });
    },

    updateChannels(state, channels) {
        channels.forEach((channel, index) => {
            Vue.set(state.channels, channel.id, channel);
        });
    },

    updateChannelUsers(state, data) {
        Vue.set(state.channel_users,data.channel_id, data.users);
    },


    showChannelSettings(state) {
        state.ui.show_channel_settings = true;
    },

    hideChannelSettings(state) {
        state.ui.show_channel_settings = false;
    },
}

export default mutations;
