var mutations = {
    addGlobalChannels(state, global_channels) {
        global_channels.forEach((global_channel, index) => {
            Vue.set(state.global_channels, global_channel.id, global_channel);
            Vue.set(state.global_channel_messages, global_channel.id, []);
            Vue.set(state.global_channel_unreads, global_channel.id, 0);
            Vue.set(state.global_channel_users, global_channel.id, []);
        });
    },

    updateGlobalChannels(state, global_channels) {
        global_channels.forEach((global_channel, index) => {
            Vue.set(state.global_channels, global_channel.id, global_channel);
        });
    },

    updateGlobalChannelUsers(state, data) {
        Vue.set(state.global_channel_users,data.channel_id, data.users);
    },


    showGlobalChannelSettings(state) {
        state.show_global_channel_settings = true;
    },

    hideGlobalChannelSettings(state) {
        state.show_global_channel_settings = false;
    },
}

export default mutations;
