var mutations = {
    selectQASection(state, module_id) {
        state.current_qa_section = module_id;
    },

    addQuestions(state, questions) {
        questions.forEach(question => {
            Vue.set(state.questions, question.id, question);
        });
    },

    selectQuestion(state, question_id) {
        state.current_question = question_id;
    },
}

export default mutations;
