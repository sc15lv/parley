var mutations = {
    addMessageGroups(state, groups) {
        groups.forEach((group, index) => {
            Vue.set(state.message_groups, group.id, group);
            Vue.set(state.group_messages, group.id, []);
        })
    },

    selectWindow(state, payload) {
        state.current_selection.type = payload.type
        state.current_selection.id = payload.id
    },

    addGroupMessages(state, data) {
        state.group_messages[data.group_id]=[
            ...state.group_messages[data.group_id],
            ...data.messages
        ];
    }
}

export default mutations;
