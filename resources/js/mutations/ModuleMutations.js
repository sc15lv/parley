var mutations = {
    addModules(state, modules) {
        modules.forEach((module, index) => {
            Vue.set(state.modules, module.id, module);
        });
    },
}

export default mutations;
