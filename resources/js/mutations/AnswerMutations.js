var mutations = {
    addAnswers(state, answers) {
        answers.forEach(answer => {
            Vue.set(state.answers,answer.id,answer);
        });
    },

}

export default mutations;
