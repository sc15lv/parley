const mutations = {
    toggleLeftSidebar(state, value=null) {
        state.mobile_ui.expand_left  = value || !state.mobile_ui.expand_left;
        state.mobile_ui.expand_right = false;
    },

    toggleRightSidebar(state, value=null) {
        state.mobile_ui.expand_right = value || !state.mobile_ui.expand_right;
        state.mobile_ui.expand_left  = false;
    },
    toggleUserModal(state, value=null) {
        state.ui.show_user_list = value || !state.ui.show_user_list;
    },
    toggleChannelsModal(state, value=null) {
        state.ui.show_channel_list = value || !state.ui.show_channel_list;
    }
};

export default mutations;
