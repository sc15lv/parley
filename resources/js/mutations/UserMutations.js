var mutations = {
    addUser(state, user) {
        Vue.set(state.users, user.id, user);
    },

    //USERS
    updateUserData(state, user) {
        state.user = user;
        Vue.set(state.users, user.id, user);
    },

    updateCSRFToken(state, csrf_token) {
        state.csrf = csrf_token;
    }
}

export default mutations;
