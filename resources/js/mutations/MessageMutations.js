var mutations = {
    addMessage(state, payload) {
        if (state.channel_messages[payload.message.target_id]) {
            state.channel_messages[payload.message.target_id] = [
                ...state.channel_messages[payload.message.target_id],
                payload.message
            ];
            if (state.current_selection.id !== payload.message.target_id) state.channel_unreads[payload.message.target_id]++;
        }
    },

    addMessages(state, data) {
        state.channel_messages[data.channel_id] = [
            ...state.channel_messages[data.channel_id],
            ...data.messages
        ];
    },

    addGlobalMessages(state, data) {
        state.global_channel_messages[data.channel_id] = [
            ...state.global_channel_messages[data.channel_id],
            ...data.messages
        ];
    },

    addDirectMessages(state, data) {
        let current_user_id = state.user.id;
        data.messages.forEach(message => {
            let key = message.sender_id == current_user_id ? message.target_id : message.sender_id;
            if (!state.direct_messages[key]) Vue.set(state.direct_messages, key, []);
            state.direct_messages[key] = [
                ...state.direct_messages[key],
                message
            ]
        });
    },

    initiateDirectMessage(state, data) {
        if (!state.direct_messages[data.recipient_id]) Vue.set(state.direct_messages, data.recipient_id, []);
    }


}

export default mutations;
