require('../bootstrap');

var actions = {
    updateGlobalChannelUsers(context, channel_id) {
        return axios.get('/global-channels/'+channel_id+'/who').then(response => {
            context.commit('updateGlobalChannelUsers', {channel_id, users: response.data});
        });
    },

    updateGlobalChannelTimestamp(context, payload) {
        return axios.post('/global-channels/' + payload.channel_id+'/updateMarker').then(response => {
            context.commit('updateGlobalChannels', [response.data]);
            return response;
        });
    },

    kickUser(context, payload) {
        return axios.get('/global-channels/'+payload.channel_id+'/kick/'+payload.user_id).then(response => {
            context.dispatch('updateGlobalChannelUsers', payload.channel_id);
        });
    },

    selectGlobalChannel(context, payload) {
        if (!payload.skip_message_refresh) {
            context.dispatch('fetchGlobalMessages', {channel_id: payload.channel_id}).then(() => {
                context.dispatch('updateGlobalChannelTimestamp', {
                    channel_id: payload.channel_id
                });
            });
        }

        if (!payload.skip_update_info) {
            context.dispatch('updateGlobalChannelUsers', payload.channel_id);
        }


        context.commit('selectWindow', {type: 'global_channel', id: payload.channel_id});
    },

    fetchGlobalChannels(context, payload) {
        return axios.get('/global-channels').then(response => {
            context.commit('addGlobalChannels', response.data)
            return response.data;
        });
    },

    setGlobalChannelPermissions(context, payload) {
        return axios.post('/global-channels/'+payload.channel_id+'/user-permissions', {
            permissions:payload.permissions,
            user_id: payload.user_id
        });
    },

    joinGlobalChannel(context, channel_id) {
        return axios.post(`/global-channels/${channel_id}/join`).then(response => {
            context.dispatch("fetchGlobalChannels");
        })
    },

    createGlobalChannel(context, payload) {
        return axios.post('/global-channels', {
            name: payload.name,
            topic: payload.topic,
            public: payload.public
        }).then(response => {
            context.dispatch("fetchGlobalChannels");
        });
    }
}

export default actions;
