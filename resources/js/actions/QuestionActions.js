require('../bootstrap');

var actions = {
    fetchQuestions(context) {
        return axios.get('/questions').then(response => {
            context.commit('addQuestions', response.data);
        });
    },

    postQuestion(context, question) {
        return axios.post('questions', question).then(response => {
            context.commit('addQuestions', [response.data]);
            return response.data;
        })
    },
}

export default actions;
