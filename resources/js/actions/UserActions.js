require('../bootstrap');

var actions = {
    getUserData(context, user_id) {
        return axios.get('/users/'+user_id).then(response => {
            context.commit('addUser', response.data);
            return response.data;
        })
    },
}

export default actions;
