require('../bootstrap');

var actions = {
    fetchMessageGroups(context, payload) {
        return axios.get('/message-groups').then((response) => {
            context.commit("addMessageGroups", response.data)
            response.data.forEach(group => {
                Echo.private(group.broadcast_channel).listen('MessageSent', (e) => {
                    context.commit('addGroupMessages', {messages: [e.message], group_id: group.id});
                });
                context.dispatch('fetchGroupMessages', {group_id: group.id});
            })

        })
    },

    selectMessageGroup(context, payload){
        context.commit("selectWindow", {
            type: "group",
            id: payload.group_id
        })
    },

    fetchGroupMessages(context, payload){
        return axios.get('/message-groups/' + payload.group_id + '/messages').then(response => {
            context.commit('addGroupMessages', {
                group_id: payload.group_id,
                messages: response.data
            })
        })
    }
}

export default actions;
