require('../bootstrap');

var actions = {
    sendMessage(context, message) {

        let url = '/';
        switch(message.target_type) {
            case 'App\\Channel':
                url = '/channels/' + message.target_id + '/messages'
                break;
            case 'App\\GlobalChannel':
                url = '/global-channels/' + message.target_id + '/messages'
                break;
            case 'App\\MessageGroup':
                url = '/message-groups/' + message.target_id + '/messages'
                break;
            case 'App\\User':
                url = '/users/' + message.target_id + '/messages'
                break;
            default:
                throw "Error";
        }
        return axios.post(url, message).then(response => {
        }).catch(error=> {
            switch (error.response.status) {
                case 403:
                    swal("Sorry!", error.response.data, "error");
                    break;
                default:
                    break;
            }
        });
    },

    fetchMessages(context, payload) {
        return axios.get('/channels/' + payload.channel_id + '/messages').then(response => {
            context.commit('addMessages', {
                messages: response.data,
                channel_id: payload.channel_id
            });
            return response;
        });
    },

    fetchGlobalMessages(context, payload) {
        return axios.get('/global-channels/' + payload.channel_id + '/messages').then(response => {
            context.commit('addGlobalMessages', {
                messages: response.data,
                channel_id: payload.channel_id
            });
            return response;
        });
    },

    fetchDirectMessages(context, payload) {
        return axios.get('/users/me/messages').then(response => {
            context.dispatch('addDirectMessages', {messages: response.data});
        })
    },

    messageUser(context, payload) {
        if (!context.state.users[payload.recipient_id]) {
            context.dispatch('getUserData', payload.recipient_id).then(() => {
                context.commit('selectWindow', {
                    type: 'user',
                    id: payload.recipient_id
                });
                context.commit('initiateDirectMessage', payload);
            })
        } else context.commit('selectWindow', {
            type: 'user',
            id: payload.recipient_id
        });
    },

    addDirectMessages(context, payload) {
        let user_ids = [];
        payload.messages.forEach(message => {
            if (!user_ids.includes(message.sender_id)) user_ids.push(message.sender_id);
            if (!user_ids.includes(message.target_id)) user_ids.push(message.target_id);
        })
        user_ids.forEach(id => (context.dispatch('getUserData', id)));
        context.commit('addDirectMessages', payload);
    }

}

export default actions;
