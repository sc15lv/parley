require('../bootstrap');

var actions = {
    fetchAnswersForQuestion(context, question_id) {
        return axios.get('/questions/'+question_id+'/answers').then(response => {
            context.commit('addAnswers', response.data);
            response.data.forEach(answer => {
                if (!context.state.users[answer.sender_id]) context.dispatch('getUserData', answer.sender_id)
            });
        })
    },
}

export default actions;
