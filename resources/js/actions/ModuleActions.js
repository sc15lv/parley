require('../bootstrap');

var actions = {
    fetchModules(context) {
        return axios.get('/modules').then(response => {
            context.commit('addModules', response.data);

        })
    },

    fetchModuleChannels(context) {
        return axios.get('/modules/channels').then(response => {
            context.commit('addChannels', response.data);
            return response;
        })
    },
}

export default actions;
