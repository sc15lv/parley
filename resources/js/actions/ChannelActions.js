require('../bootstrap');

var actions = {
    updateChannelUsers(context, channel_id) {
        return axios.get('/channels/'+channel_id+'/who').then(response => {
            context.commit('updateChannelUsers', {channel_id, users: response.data});
        })
    },

    updateChannelTimestamp(context, payload) {
        return axios.post('channels/' + payload.channel_id+'/updateMarker').then(response => {
            context.commit('updateChannels', [response.data]);
            return response;
        });
    },

    kickUser(context, payload) {
        return axios.get('channels/'+payload.channel_id+'/kick/'+payload.user_id).then(response => {
            context.dispatch('updateChannelUsers', payload.channel_id);
        });
    },

    selectChannel(context, payload) {
        if (!payload.skip_message_refresh) {
            context.dispatch('fetchMessages', {channel_id: payload.channel_id}).then(() => {
                context.dispatch('updateChannelTimestamp', {
                    channel_id: payload.channel_id
                });
            });
        }

        if (!payload.skip_update_info) {
            context.dispatch('updateChannelUsers', payload.channel_id);
        }


        context.commit('selectWindow', {type: 'channel', id: payload.channel_id});
    },

    fetchChannels(context, payload) {
        return axios.get('/channels').then(response => {
            context.commit('addChannels', response.data)
            return response.data;
        });
    },

    setChannelPermissions(context, payload) {
        return axios.post('/channels/'+payload.channel_id+'/user-permissions', {
            permissions:payload.permissions,
            user_id: payload.user_id
        });
    },
}

export default actions;
