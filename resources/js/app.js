
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import Vuex from 'vuex';
Vue.use(Vuex);
import moment from 'moment';
window.moment = moment;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

var Octicon = require('vue-octicons');
Vue.component('octicon', Octicon);

//Import all mutations
import MessageMutations from './mutations/MessageMutations';
import ChannelMutations from './mutations/ChannelMutations';
import GlobalChannelMutations from './mutations/GlobalChannelMutations';
import AnswerMutations from './mutations/AnswerMutations';
import ModuleMutations from './mutations/ModuleMutations';
import QuestionMutations from './mutations/QuestionMutations';
import UserMutations from './mutations/UserMutations';
import MessageGroupMutations from './mutations/MessageGroupMutations';
import UIMutations from './mutations/UIMutations';

//Import all actions
import MessageGroupActions from './actions/MessageGroupActions';
import ChannelActions from './actions/ChannelActions';
import GlobalChannelActions from './actions/GlobalChannelActions';
import AnswerActions from './actions/AnswerActions';
import QuestionActions from './actions/QuestionActions';
import MessageActions from './actions/MessageActions';
import ModuleActions from './actions/ModuleActions';
import UserActions from './actions/UserActions';

//setup Vuex store:
const store = new Vuex.Store({
    state: {
        user: null,
        users: {},
        current_question: null,
        answers: {},
        direct_messages: {},
        message_groups: {},
        group_messages: {},
        channels: {},
        channel_users: {},
        channel_unreads: {},
        channel_messages: {},
        global_channels: {},
        global_channel_users: {},
        global_channel_unreads: {},
        global_channel_messages: {},
        modules: {},
        questions: {},
        question_votes: {},
        current_selection: {
            type: 'channel',
            id: null
        },
        csrf: null,
        mobile_ui: {
            expand_left: false,
            expand_right: false
        },
        ui: {
            show_channel_settings: false,
            show_channel_list: false,
            show_user_list: false,
        }
    },

    mutations: {
        ...MessageMutations,
        ...ChannelMutations,
        ...GlobalChannelMutations,
        ...ModuleMutations,
        ...QuestionMutations,
        ...AnswerMutations,
        ...MessageGroupMutations,
        ...UserMutations,
        ...UIMutations
    },

    actions: {
        ...MessageActions,
        ...ChannelActions,
        ...GlobalChannelActions,
        ...ModuleActions,
        ...UserActions,
        ...QuestionActions,
        ...AnswerActions,
        ...MessageGroupActions,
    },

    getters: {
        messages: (state) => (channel_id) => {
            return Object.values(state.channel_messages[channel_id]).sort((a, b) => {
                let ma = moment(a.created_at, "YYYY-MM-DD HH-mm-ss");
                let mb = moment(b.created_at, "YYYY-MM-DD HH-mm-ss");
                if (ma.isBefore(mb)) return -1;
                else if (ma.isAfter(mb)) return -1;
                else return 0;
            })
        },

        getUserByID: (state) => (id) => {
            let user = state.users[id];
            if (!user) store.dispatch('getUserData', id).then(user => {
                return user;
            })
            return user;
        },

        channelPermissions: (state) => (channel_id) => {
            return (({send_message, read_any_message, delete_own_message, delete_any_message, edit_any_message, invite_user, kick_user, ban_user, leave_channel, edit_channel_settings}) => ({send_message, read_any_message, delete_own_message, delete_any_message, edit_any_message, invite_user, kick_user, ban_user, leave_channel, edit_channel_settings}))(state.channels[channel_id].pivot);
        }
    }
})




/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    propsData: ['csrf'],
    data: {

    },
    store,

    created() {
        this.$store.commit('updateCSRFToken', this.csrf);
       if (window.location.pathname === "/home" || window.location.pathname === "/chat") this.initialiseChat();

    },

    methods: {
        initialiseChat() {
            this.fetchUserData().then(() => {
                this.$store.dispatch('fetchMessageGroups');
                this.$store.dispatch('fetchDirectMessages');
            });

            this.$store.dispatch('fetchChannels').then(channels => {
                channels.forEach(channel => {
                    Echo.private(channel.id+channel.name).listen('MessageSent', (e) => {
                        store.commit('addMessages', {messages: [e.message], channel_id: channel.id});
                    });
                })
                if (Object.keys(this.$store.state.channels).length) {
                    let first_channel_id = Object.keys(this.$store.state.channels)[0];
                    this.$store.dispatch('selectChannel', {
                        channel_id: first_channel_id,

                    });
                }

            });

            this.$store.dispatch('fetchGlobalChannels').then(channels => {
                channels.forEach(channel => {
                    Echo.private("G"+channel.id+channel.name).listen('MessageSent', (e) => {
                        store.commit('addGlobalMessages', {messages: [e.message], channel_id: channel.id});
                    });
                })
                if (Object.keys(this.$store.state.global_channels).length) {
                    let first_channel_id = Object.keys(this.$store.state.global_channels)[0];
                    this.$store.dispatch('selectGlobalChannel', {
                        channel_id: first_channel_id,

                    });
                }

            });

            this.$store.dispatch('fetchModules').then(() => {
                this.$store.dispatch('fetchQuestions');
            })
        },

        fetchUserData() {
            return axios.get('/users/me').then(response => {
                store.commit('updateUserData', response.data);
                Echo.private(response.data.broadcast_channel).listen('MessageSent', (e) => {
                    store.commit('addDirectMessages', {messages: [e.message]});
                });
            })
        }
    }
});
