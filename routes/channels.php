<?php

use App\User;
use App\Channel;
use App\MessageGroup;
use App\GlobalChannel;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/




foreach (Channel::all() as $channel) {
    Broadcast::channel($channel->getBroadcastChannelName(), function ($current_user) use ($channel) {
        return $current_user->canDoInChannel('read_any_message', $channel->id);
    });
}

foreach (GlobalChannel::all() as $channel) {
    Log::info(sprintf("%s, %d, %s", $channel->name, $channel->id, $channel->getBroadcastChannelName()));
    Broadcast::channel($channel->getBroadcastChannelName(), function ($current_user) use ($channel) {
        return $current_user->canDoInGlobalChannel('read_any_message', $channel->id);
    });
}
Log::info("END");

foreach (User::all() as $user) {
    Broadcast::channel($user->getBroadcastChannelName(), function ($current_user) use ($user) {
        return $current_user->id === $user->id;
    });
}

foreach (MessageGroup::all() as $group) {
    Broadcast::channel($group->getBroadcastChannelName(), function ($current_user) use ($group) {
        return in_array($current_user->id, $group->users->pluck('id')->toArray());
    });
}

