<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['update-timestamp', 'auth'])->group(function() {
    Route::get('/home',                                         'MessageController@index')->name('home');

    Route::get('users/me',                                      'UserController@getOwnUserData');
    Route::get('users/{user}',                                  'UserController@getUserData');

    Route::get('messages',                                      'MessageController@fetchMessages');
    Route::post('messages',                                     'MessageController@sendMessage');

    Route::post('users/{user}/messages',                        'UserController@sendMessage');
    Route::get('users/me/messages',                             'UserController@getMessages');


    Route::get('message-groups',                                'MessageGroupController@index');
    Route::post('message-groups/{group}/messages',              'MessageGroupController@sendMessage');
    Route::get('message-groups/{group}/messages',               'MessageGroupController@getMessages');


    //ChannelController
    Route::get('channels',                                      'ChannelController@index');

    Route::get('channels/{channel}/messages',                   'ChannelController@getMessages');
    Route::post('channels/{channel}/messages',                  'ChannelController@sendMessage');

    Route::get('channels/{channel}/user-permissions',           'ChannelController@getUserPermissions');
    Route::post('channels/{channel}/user-permissions',          'ChannelController@setUserPermissions');

    Route::get('channels/{channel}/kick/{user}',                'ChannelController@kickUser');
    Route::post('channels/{channel}/updateMarker',              'ChannelController@updateLastVisit');
    Route::get('channels/{channel}/who',                        'ChannelController@onlineUsers');

    //GlobalChannelController
    Route::get('global-channels',                                      'GlobalChannelController@index');
    Route::post('global-channels',                                     'GlobalChannelController@store');
    Route::get('global-channels/all',                                  'GlobalChannelController@allPublicChannels');

    Route::get('global-channels/{channel}/messages',                   'GlobalChannelController@getMessages');
    Route::post('global-channels/{channel}/messages',                  'GlobalChannelController@sendMessage');

    Route::get('global-channels/{channel}/user-permissions',           'GlobalChannelController@getUserPermissions');
    Route::post('global-channels/{channel}/user-permissions',          'GlobalChannelController@setUserPermissions');

    Route::get('global-channels/{channel}/kick/{user}',                'GlobalChannelController@kickUser');
    Route::post('global-channels/{channel}/updateMarker',              'GlobalChannelController@updateLastVisit');
    Route::get('global-channels/{channel}/who',                        'GlobalChannelController@onlineUsers');
    Route::post('global-channels/{channel}/join',                      'GlobalChannelController@joinChannel');


    Route::get('modules',                                       'ModuleController@index');
    Route::get('modules/channels',                              'ChannelController@moduleChannels');

    Route::get('questions',                                     'QuestionController@index');
    Route::get('questions/{question}/answers',                  'AnswerController@answersForQuestion');
    Route::post('questions',                                    'QuestionController@store');
    Route::post('questions/{question}/answers',                 'AnswerController@answerQuestion');
    Route::post('answers/{answer}/upvote',                      'AnswerController@upvote');
});

