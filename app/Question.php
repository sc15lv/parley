<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'title',
        'text',
        'module_id',
    ];

    protected $with = [
        'votes.user',
    ];

    public function answers() {
        return $this->hasMany('App\Answer');
    }

    public function chosenAnswer() {
        return $this->hasOne('App\Answer');
    }

    public function votes() {
        return $this->morphMany('App\Vote', 'subject');
    }
}
