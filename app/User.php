<?php

namespace App;

use App\Vote;
use App\Answer;
use App\Module;
use Carbon\Carbon;
use App\MessageGroup;
use App\GlobalChannel;
use App\Interfaces\Broadcastable;
use Illuminate\Support\Collection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

define('USER_PERMISSIONS', [
    'send_message',
    'read_any_message',
    'delete_any_message',
    'edit_any_message',
    'invite_user',
    'kick_user',
    'ban_user',
    'leave_channel',
    'edit_channel_settings'
]);

class User extends Authenticatable implements Broadcastable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_seen' => 'datetime'
    ];

    protected $appends = [
        'total_score',
        'is_online'
    ];


    //Relationships

    public function sentMessages() {
        return $this->hasMany(Message::class, 'sender_id');
    }

    public function receivedMessages() {
        return $this->morphMany(Message::class, 'target');
    }

    public function questions() {
        return $this->hasMany(Question::class, 'sender_id');
    }

    public function answers() {
        return $this->hasMany(Answer::class, 'sender_id');
    }

    public function channels() {
        return $this->belongsToMany(Channel::class, 'channel_user', 'user_id');
    }

    public function globalChannels() {
        return $this->belongsToMany(GlobalChannel::class, 'global_channel_user', 'user_id');
    }

    public function moduleChannels() {
        // return $this->modules()->with('channels')->get();
        $channels = new Collection();
        foreach($this->modules as $module) {
            // $module->load('channels');
            $channels = $channels->merge($module->channels);
        }
        return $channels;
    }

    public function votes() {
        return $this->hasMany(Vote::class);
    }

    public function modules() {
        return $this->belongsToMany(Module::class, 'module_user')->withPivot('role');
    }

    public function messageGroups() {
        return $this->belongsToMany(MessageGroup::class)->withPivot('last_visit');
    }



    //Attributes

    public function getTotalScoreAttribute() {
        return $this->hasManyThrough(Vote::class, Question::class, 'sender_id', 'subject_id')->get()->count() + ($this->hasManyThrough('App\Vote', 'App\Answer', 'sender_id', 'subject_id')->get()->count()*2);
    }

    //Scopes



    //Non-eloquent methods:
    public function canDoInChannel($permission, int $channel_id) {
        $user_channel = $this->channels()->withPivot(USER_PERMISSIONS)->find($channel_id);
        if ($user_channel) {
            return $user_channel->pivot[$permission];
        }
        else return false;
    }

    //Non-eloquent methods:
    public function canDoInGlobalChannel($permission, int $channel_id) {
        $user_channel = $this->globalChannels()->withPivot(USER_PERMISSIONS)->find($channel_id);
        if ($user_channel) {
            return $user_channel->pivot[$permission];
        }
        else return false;
    }

    public function updateLastSeen() {
        $this->last_seen = now();
        $this->save();
    }

    public function getBroadcastChannelAttribute() {
        return $this->getBroadcastChannelName();
    }

    public function getIsOnlineAttribute() {
        return $this->last_seen != null ?Carbon::instance($this->last_seen)->diffInMinutes(new Carbon()) < 5 : false;
    }

    public function getBroadcastChannelName()
    {
        return "@@DMS-".$this->id;
    }
}
