<?php

namespace App;

use App\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Broadcastable;

class Channel extends Model implements Broadcastable
{
    protected $fillable = [
        'module_id',
        'name',
        'topic'
    ];

    protected $appends = [
        'permissions'
    ];

    public function users() {
        return $this->belongsToMany(User::class);
    }

    public function messages() {
        return $this->morphMany(Message::class, 'target');
    }

    public function module() {
        return $this->belongsTo('App\Module');
    }

    public function getPermissionsAttribute() {
        if (!$this->users->contains(Auth::user())) {
            return [
                'send_message' => false,
                'read_any_message' => false,
                'delete_own_message' => false,
                'delete_any_message' => false,
                'edit_any_message' => false,
                'invite_user' => false,
                'kick_user' => false,
                'ban_user' => false,
                'leave_channel' => false,
                'edit_channel_settings' => false,
            ];
        }

        $pivot_data =  $this->users()->where('users.id', Auth::user()->id)->withPivot(USER_PERMISSIONS)->first()->pivot;
        return [
            'send_message' => $pivot_data['send_message'],
            'read_any_message' => $pivot_data['read_any_message'],
            'delete_own_message' => $pivot_data['delete_own_message'],
            'delete_any_message' => $pivot_data['delete_any_message'],
            'edit_any_message' => $pivot_data['edit_any_message'],
            'invite_user' => $pivot_data['invite_user'],
            'kick_user' => $pivot_data['kick_user'],
            'ban_user' => $pivot_data['ban_user'],
            'leave_channel' => $pivot_data['leave_channel'],
            'edit_channel_settings' => $pivot_data['edit_channel_settings']
        ];
    }


    public function getBroadcastChannelName() {
        return $this->id.$this->name;
    }
}
