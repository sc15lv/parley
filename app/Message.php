<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'message',
        'target_id',
        'target_type',
        'sender_id'
    ];

    public function sender() {
        return $this->belongsTo(User::class);
    }

    public function messageable() {
        return $this->morphTo('target');
    }
}
