<?php
namespace App\Interfaces;

interface Broadcastable {
    public function getBroadcastChannelName();
}
