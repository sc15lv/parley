<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'text',
        'question_id'
    ];

    protected $with = [
        'votes:id,subject_id,subject_type,user_id',
        'votes.user:id,name',
    ];

    public function votes() {
        return $this->morphMany('App\Vote', 'subject');
    }

    public function question() {
        return $this->hasOne('App\Question');
    }
}
