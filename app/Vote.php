<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $fillable = [
        'subject_type',
        'subject_id'
    ];

    protected $with = [
        'user:id,name',
    ];

    public function subject() {
        return $this->morphTo('votes');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
