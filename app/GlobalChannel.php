<?php

namespace App;

use App\Channel;

class GlobalChannel extends Channel
{
    public function getBroadcastChannelName() {
        return "G".$this->id.$this->name;
    }
}
