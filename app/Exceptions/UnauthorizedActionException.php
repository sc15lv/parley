<?php
namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class UnauthorizedActionException extends HttpException
{
    /**
     * @param string     $challenge WWW-Authenticate challenge string
     * @param string     $message   The internal exception message
     * @param \Exception $previous  The previous exception
     * @param int        $code      The internal exception code
     * @param array      $headers
     */
    public function __construct(string $message = null, ?int $status_code = 403, array $headers = [])
    {
        parent::__construct($status_code, $message, null, $headers, 0);
    }
}

