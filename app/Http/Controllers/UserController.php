<?php

namespace App\Http\Controllers;

use App\User;
use App\Events\MessageSent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class UserController extends ParleyController
{
    public function __construct() {
        parent::__construct();
    }

    public function getOwnUserData(Request $request) {
        return User::find(Auth::user()->id)->append('broadcast_channel');
    }

    public function getUserData(User $user) {
        return $user->only(['name', 'id', 'photo_url', 'total_score']);
    }

    public function getMessages() {
        //TODO TEST THIS WORKS;
        // $recent_messagers_ids =  Auth::user()->receivedMessages()->groupBy('sender_id')->orderBy(DB::raw('max(created_at)'))->select('sender_id')->limit(10)->pluck('sender_id');
        $user = Auth::user();
        return \App\Message::where(function($query) use ($user){
            $query->where('sender_id', $user->id)->where('target_type', 'App\User');
        })->orWhere(function($query) use ($user) {
            $query->where('target_id', $user->id)->where('target_type', 'App\User');
        })->get()->load('sender:name,id')->sortBy('created_at');
    }

    public function sendMessage(User $user, Request $request) {
        $currentUser = Auth::user();
        $message = $user->receivedMessages()->create([
            'message' => $request->input('message'),
            'sender_id' => $currentUser->id
        ]);

        $message->load('sender');

        broadcast(new MessageSent($currentUser, $message));
        broadcast(new MessageSent($currentUser, $message, $currentUser));

        return ['status' => 'Message Sent!'];
    }
}
