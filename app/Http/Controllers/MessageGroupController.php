<?php

namespace App\Http\Controllers;

use App\MessageGroup;
use App\Events\MessageSent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageGroupController extends ParleyController
{
    public function index() {
        return Auth::User()->messageGroups()->get();
    }

/**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function sendMessage(MessageGroup $group, Request $request)
    {
        $user = Auth::user();
        if ($user->messageGroups->contains($group)) {
            $message = $group->messages()->create([
                'message' => $request->input('message'),
                'sender_id' => $user->id
            ]);

            $message->load('sender');


            broadcast(new MessageSent($user, $message));

            return ['status' => 'Message Sent!'];
        }
        else return response("You're not part of this group chat", 403);
    }


    public function getMessages(MessageGroup $group, Request $request) {
        return $group->messages->load('sender:name,id')->sortBy('created_at');
    }
}
