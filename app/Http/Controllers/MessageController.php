<?php

namespace App\Http\Controllers;

use App\Message;
use App\Events\MessageSent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends ParleyController
{
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Show chats
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('chat');
    }

    /**
     * Fetch all messages
     *
     * @return Message
     */
    public function fetchMessages()
    {
        return response(403);
    }

    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function sendMessage(Request $request)
    {
        $user = Auth::user();
        // return $user->canDoInChannel('send_message', $request->target_id);
        if ($user->canDoInChannel('send_message', $request->target_id)) {
            $message = $user->messages()->create([
                'message' => $request->input('message'),
                'target_id' => $request->input('target_id'),
                'target_type' => $request->input('target_type')
            ]);

            $message->load('sender');


            broadcast(new MessageSent($user, $message));

            return ['status' => 'Message Sent!'];
        }
        else return response("You don't have permission to send messages in this channel.", 403);

    }
}


