<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class AnswerController extends ParleyController
{
    public function __construct() {
        parent::__construct();
    }

    public function index() {

    }

    public function answersForQuestion(Question $question) {
        return $question->answers->sortBy(function($answer, $key) {
            return $answer->votes->count();
        })->values();
    }

    public function answerQuestion(Question $question, Request $request) {
        $user = Auth::user();
        $answer = $user->answers()->create([
            'text' => $request->get('text'),
            'question_id' => $question->id
        ]);

        $question->answers()->save($answer);
        $answer->load('votes');
        return $answer;
    }

    public function upvote(Answer $answer) {
        $votes = $answer->votes()->get();
        $user = Auth::user();

        $already_voted = $votes->contains(function ($value, $key) use ($user) {
            return $value['user_id'] === $user->id;
        });


        if ($already_voted){
            $answer->votes()->where('user_id', $user->id)->delete();
        } else {
            $answer->votes()->save($user->votes()->create());
        }

        $answer->load('votes');
        return $answer;

    }
}
