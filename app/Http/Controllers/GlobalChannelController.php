<?php

namespace App\Http\Controllers;

use App\User;
use App\GlobalChannel;
use App\Events\MessageSent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\UnauthorizedActionException;

class GlobalChannelController extends ParleyController
{
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        return Auth::user()->globalChannels()->get();
    }

    public function store(Request $request) {
        $validated = $request->validate([
            'name' => 'string|required',
            'topic' => 'string|required',
            'public' => 'boolean'
        ]);

        Auth::user()->globalChannels()->create($validated, [
            'send_message' => true,
            'read_any_message' => true,
            'delete_own_message' => true,
            'delete_any_message' => true,
            'edit_any_message' => true,
            'invite_user' => true,
            'kick_user' => true,
            'ban_user' => true,
            'leave_channel' => true,
            'edit_channel_settings' => true,
        ]);
    }

    public function allPublicChannels() {
        return GlobalChannel::where('public', true)->get();
    }


    public function onlineUsers(GlobalChannel $channel) {
        return $channel->users()->get()->map(function ($user) {
            return $user->only(['name', 'id', 'last_seen', 'photo_url', 'is_online']);
        });
    }

    public function getChannel(GlobalChannel $channel) {
        $user = Auth::user();

        if (in_array($channel->module_id, $user->modules->pluck('id')) || $user->channels->contains('id', $channel->id)) {
            return $channel;
        }
    }

    public function updateLastVisit(GlobalChannel $channel, Request $request) {
        Auth::user()->globalChannels()->updateExistingPivot($channel, ['last_visit' => now()]);
        return $channel;
    }

    public function getMessages(GlobalChannel $channel) {
        $user = Auth::user();
        if ($user->canDoInGlobalChannel('read_any_message', $channel->id)) {
            return $channel->messages->load('sender:name,id')->sortBy('created_at');
        } else {
            throw new UnauthorizedActionException("You don't have permission to read messages in this channel", 403);
        }
    }

    public function getUserPermissions(GlobalChannel $channel) {
        $user = Auth::user();
        if ($user->canDoInGlobalChannel('edit_channel_settings', $channel->id)) {
            return $channel->users()->withPivot(USER_PERMISSIONS, 'id')->get();
        } else {
            throw new UnauthorizedActionException("You don't have permission to edit settings in this channel", 403);
        }
    }

    public function setUserPermissions(GlobalChannel $channel, Request $request) {
        $user = Auth::user();
        $validated = $request->validate([
            'permissions' => 'required',
            'user_id' => 'required|exists:users,id'
        ]);

        if ($user->canDoInGlobalChannel('edit_channel_settings', $channel->id)) {
            DB::table('channel_user')->where('channel_id', $channel->id)->where('user_id', $validated['user_id'])->update($validated['permissions']);
            return $channel->users()->withPivot(USER_PERMISSIONS, 'id')->get();
        } else {
            throw new UnauthorizedActionException("You don't have permission to edit settings in this channel", 403);
        }
    }

    public function kickUser(GlobalChannel $channel, User $user) {
        $currentUser = Auth::user();

        if ($currentUser->canDoInGlobalChannel('kick_user', $channel->id)) {
            $channel->users()->detach($user);
        }
        else {
            throw new UnauthorizedActionException("You don't have permission to kick users in this channel", 403);
        }
    }

    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function sendMessage(GlobalChannel $channel, Request $request)
    {
        $user = Auth::user();
        // return $user->canDoInGlobalChannel('send_message', $request->target_id);
        if ($user->canDoInGlobalChannel('send_message', $channel->id)) {
            $message = $channel->messages()->create([
                'message' => $request->input('message'),
                'sender_id' => $user->id
            ]);

            $message->load('sender');

            broadcast(new MessageSent($user, $message));

            return ['status' => 'Message Sent!'];
        }
        else return response("You don't have permission to send messages in this channel.", 403);
    }

    public function joinChannel(GlobalChannel $channel) {
        if ($channel->public) {
            Auth::user()->globalChannels()->save($channel, [
                'send_message' => true,
                'read_any_message' => true,
                'delete_own_message' => true,
                'delete_any_message' => false,
                'edit_any_message' => false,
                'invite_user' => false,
                'kick_user' => false,
                'ban_user' => false,
                'leave_channel' => true,
                'edit_channel_settings' => false,
            ]);
        } else return response("This channel is not public, an invitation is required to join.", 403);
    }
}
