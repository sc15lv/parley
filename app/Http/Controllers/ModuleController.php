<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class ModuleController extends ParleyController
{
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        return Auth::user()->modules()->get();
    }
}
