<?php

namespace App\Http\Controllers;

use App\Module;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class QuestionController extends ParleyController
{
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $questions = new Collection();
        foreach(Auth::user()->modules as $module) {
            $questions = $questions->merge($module->questions);
        }
        return $questions;
    }

    public function store(Request $request) {
        $user = Auth::user();
        $question = $user->questions()->create([
            'title' => $request->get('title'),
            'text' => $request->get('text'),
            'module_id' => $request->get('module_id')
        ]);

        $question->save();
        return $question;
    }
}
