<?php

namespace App\Http\Controllers;

use App\User;
use App\Channel;
use App\Events\MessageSent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class ChannelController extends ParleyController
{
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        return Auth::user()->channels()->get();
    }

    public function moduleChannels() {
        return Auth::user()->moduleChannels();
    }

    public function onlineUsers(Channel $channel) {
        return $channel->users()->get()->map(function ($user) {
            return $user->only(['name', 'id', 'last_seen', 'photo_url', 'is_online']);
        });
    }

    public function getChannel(Channel $channel) {
        $user = Auth::user();

        if (in_array($channel->module_id, $user->modules->pluck('id')) || $user->channels->contains('id', $channel->id)) {
            return $channel;
        }
    }

    public function updateLastVisit(Channel $channel, Request $request) {
        Auth::user()->channels()->updateExistingPivot($channel, ['last_visit' => now()]);
        return $channel;
    }

    public function getMessages(Channel $channel) {
        $user = Auth::user();
        if ($user->canDoInChannel('read_any_message', $channel->id)) {
            return $channel->messages->load('sender:name,id')->sortBy('created_at');
        } else {
            throw new UnauthorizedHttpException("", "You don't have permission to read messages in this channel");
        }
    }

    public function getUserPermissions(Channel $channel) {
        $user = Auth::user();
        if ($user->canDoInChannel('edit_channel_settings', $channel->id)) {
            return $channel->users()->withPivot(USER_PERMISSIONS, 'id')->get();
        } else {
            throw new UnauthorizedHttpException("", "You don't have permission to edit settings in this channel");
        }
    }

    public function setUserPermissions(Channel $channel, Request $request) {
        $user = Auth::user();
        $validated = $request->validate([
            'permissions' => 'required',
            'user_id' => 'required|exists:users,id'
        ]);

        if ($user->canDoInChannel('edit_channel_settings', $channel->id)) {
            DB::table('channel_user')->where('channel_id', $channel->id)->where('user_id', $validated['user_id'])->update($validated['permissions']);
            return $channel->users()->withPivot(USER_PERMISSIONS, 'id')->get();
        } else {
            throw new UnauthorizedHttpException("", "You don't have permission to edit settings in this channel");
        }
    }

    public function kickUser(Channel $channel, User $user) {
        $currentUser = Auth::user();

        if ($currentUser->canDoInChannel('kick_user', $channel->id)) {
            $channel->users()->detach($user);
        }
        else {
            throw new UnauthorizedHttpException("", "You don't have permission to kick users in this channel");
        }
    }

    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function sendMessage(Channel $channel, Request $request)
    {
        $user = Auth::user();
        // return $user->canDoInChannel('send_message', $request->target_id);
        if ($user->canDoInChannel('send_message', $channel->id)) {
            $message = $channel->messages()->create([
                'message' => $request->input('message'),
                'sender_id' => $user->id
            ]);

            $message->load('sender');


            broadcast(new MessageSent($user, $message));

            return ['status' => 'Message Sent!'];
        }
        else return response("You don't have permission to send messages in this channel.", 403);

    }
}
