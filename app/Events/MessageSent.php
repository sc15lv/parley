<?php

namespace App\Events;

use App\User;
use App\Message;
use App\Channel as ParleyChannel;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class MessageSent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $sender;
    public $message;
    public $target;
    public $override_target;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $sender, Message $message, $override_target=null) {
        $this->sender = $sender;
        $this->message = $message;
        $this->target = $override_target ?:$message->messageable;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn() {
        return new PrivateChannel($this->target->getBroadcastChannelName());
    }
}
