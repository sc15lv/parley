<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Broadcastable;

class MessageGroup extends Model implements Broadcastable
{
    protected $appends=[
        'broadcast_channel'
    ];

    //Relationships
    public function users() {
        return $this->belongsToMany(User::class);
    }

    public function messages() {
        return $this->morphMany(Message::class, 'target');
    }

    public function getBroadcastChannelAttribute() {
        return $this->getBroadcastChannelName();
    }

    //Non-ORM:
    public function getBroadcastChannelName() {
        return "@@GM-". join('-', $this->users()->pluck('id')->toArray());
    }
}
