<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public function channels() {
        return $this->hasMany('App\Channel');
    }

    public function users() {
        return $this->belongsToMany('App\User');
    }

    public function questions() {
        return $this->hasMany('App\Question');
    }

    public function acceptedAnswer()  {
        return $this->hasOne('App\Question', 'answer_id');
    }
}
