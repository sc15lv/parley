<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('sender_id')->unsigned(); //user who sent message
            $table->foreign('sender_id')->references('id')->on('users');

            $table->text('target_id')->nullable(); //Channel, user, or group ID.
            $table->integer('target_type')->unsigned()->nullable();//'Channel', 'User', 'Group'

            $table->text('message'); // Message contents
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages', function(Blueprint $table) {
            $table->dropForeign('sender_id');
        });
        Schema::dropIfExists('messages');
    }
}
