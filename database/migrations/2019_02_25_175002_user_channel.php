<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserChannel extends Migration
{
    private $permissions = [
        'send_message',
        'read_any_message',
        'delete_own_message',
        'delete_any_message',
        'edit_any_message',
        'invite_user',
        'kick_user',
        'ban_user',
        'leave_channel',
        'edit_channel_settings'
    ];


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('channel_user', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('channel_id')->unsigned();
            $table->foreign('channel_id')->references('id')->on('channels');

            $table->boolean('private')->default(true);

            foreach ($this->permissions as $permission) {
                $table->boolean($permission)->default(false);
            }


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('channel_user', function(Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['channel_id']);

        });
        Schema::dropIfExists('channel_user');
    }
}
