<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCascadeToChannelUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('channel_user', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['channel_id']);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->foreign('channel_id')->references('id')->on('channels')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('channel_user', function (Blueprint $table) {
            $table->dropForeign(['user_id', 'channel_id']);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('channel_id')->references('id')->on('channels');
        });
    }
}
